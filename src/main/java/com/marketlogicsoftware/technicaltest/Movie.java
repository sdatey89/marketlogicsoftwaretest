package com.marketlogicsoftware.technicaltest;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.MediaType;
import java.util.ArrayList;
import java.util.Collections;


public class Movie {

    static String[] getMovieTitles(String substr) {
        String targetUrlForPage1 = "https://jsonmock.hackerrank.com/api/movies/search/?Title="+substr+"&page=1";
        String targetUrlForPage2 = "https://jsonmock.hackerrank.com/api/movies/search/?Title="+substr+"&page=2";

        Client client = ClientBuilder.newClient();

        Movies moviesFromPage1 = client
                .target(targetUrlForPage1)
                .request(MediaType.APPLICATION_JSON)
                .get(Movies.class);
        Movies moviesFromPage2 = client
                .target(targetUrlForPage2)
                .request(MediaType.APPLICATION_JSON)
                .get(Movies.class);

        ArrayList<String> movieList= new ArrayList();
        for(Data data : moviesFromPage1.getData()){
            movieList.add( data.getTitle());
        }
        for(Data data : moviesFromPage2.getData()){
            movieList.add( data.getTitle());
        }
        Collections.sort(movieList);

        return movieList.toArray(new String[movieList.size()]);
    }
    public static void main(String args[]){
        for(String movieTitle: getMovieTitles("spiderman")){
            System.out.println(movieTitle);
        }
    }
}
