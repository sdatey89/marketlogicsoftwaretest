package com.marketlogicsoftware.technicaltest;

public class OffsetEncodingUtility {
    public static String encode(int offset, String original) {
        return original.chars().map(c -> {
                    if(Character.isLetter(c)){
                        c = (char) ((int)c + offset);
                        c = (offset == 1 && c == 91) ? (char) 65 : (offset == 1 && c == 123) ? (char) 97 : c ;
                        c = (offset == -1 && c == 64) ? (char) 90 : (offset == -1 && c == 96) ? (char) 122 : c;
                        return c;
                    }else{
                        return c;
                    }
                }).collect( StringBuilder::new,
                        StringBuilder::appendCodePoint,
                        StringBuilder::append )
                .toString();
    }

}
