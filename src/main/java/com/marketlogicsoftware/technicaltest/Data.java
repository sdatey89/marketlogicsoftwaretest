package com.marketlogicsoftware.technicaltest;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Data {
    @JsonProperty("Poster")
    private String poster;
    @JsonProperty("Type")
    private String type;
    @JsonProperty("Title")
    private String title ;
    @JsonProperty("Year")
    private int year;
    @JsonProperty("imdbID")
    private String imdbID;


    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }
}
