package com.marketlogicsoftware.technicaltest;

import org.junit.Test;

import static org.junit.Assert.*;

public class OffsetEncodingUtilityTest {
    public static final String ENGLISTTEXT =
            "My Name is Anthony. How do you do? Where is zebra, in the Zoo!";
    public static final String EXPECTED_POSITIVE_OFFSET_TEXT =
            "Nz Obnf jt Bouipoz. Ipx ep zpv ep? Xifsf jt afcsb, jo uif App!";
    public static final String EXPECTED_NEGATIVE_OFFSET_TEXT =
            "Lx Mzld hr Zmsgnmx. Gnv cn xnt cn? Vgdqd hr ydaqz, hm sgd Ynn!";
    public static final String NONALPHABET_TEXT = "!@129$#^%&(*)}{:?><";

    @Test
    public void encodeEnglishTextWithPositiveOffset() {
        assertEquals(EXPECTED_POSITIVE_OFFSET_TEXT, OffsetEncodingUtility.encode(1, ENGLISTTEXT));
    }

    @Test
    public void encodeEnglishTextWithNegativeOffset() {
        assertEquals(EXPECTED_NEGATIVE_OFFSET_TEXT, OffsetEncodingUtility.encode(-1, ENGLISTTEXT));
    }

    @Test
    public void noEncodeOnNonAlphabets(){
        assertEquals(NONALPHABET_TEXT, OffsetEncodingUtility.encode(1,NONALPHABET_TEXT));
    }

    @Test
    public void noEncodeOnNonAlphabetsForNegativeOffset(){
        assertEquals(NONALPHABET_TEXT, OffsetEncodingUtility.encode(-1,NONALPHABET_TEXT));
    }
}